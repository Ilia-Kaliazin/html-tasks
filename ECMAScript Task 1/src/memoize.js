// Completed all point of task.
export function memoize(callback) {
  if(typeof callback != 'function') return null;

  let cache = new Map();
  return function(...args) {
    for (let key of cache.keys())
      if(key.every((i,j)=>i == args[j])) return cache.get(key);

    cache.set(args, callback.call(this, ...args));
    return cache.get(args);
  }
}
