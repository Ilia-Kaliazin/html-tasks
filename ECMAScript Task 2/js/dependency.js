/* Dependency */
let table = document.getElementById("table"); // Input table.
let tableRow = table.getElementsByTagName("td");   // Init point of table.
let checkboxArray = document.querySelectorAll("input[type=checkbox]");

function createElement(array, _id) {
  let sizeArray = array.length;
  let row = document.createElement("tr");
  // Create template element.
  for (var i = 0; i < sizeArray; i++)
    row.appendChild(document.createElement("td"));

  row.id = _id;
  table.appendChild(row);
  let tableRowCount = tableRow.length;
  // Fill element.
  for (let i = 0; i < sizeArray; i++) {
    if('object' == typeof array[i]) (function(){
      for (let j = 1; j < array[i].length; j++)
        style(i, array[i][j]);
      innerHTML(i, array[i][0]);
    }());
    else innerHTML(i, array[i]);
  }

  function innerHTML(point, value) {
    tableRow[tableRowCount - sizeArray + point].innerHTML = value;
  }

  function style(point, style) {
    tableRow[tableRowCount - sizeArray + point].style = style;
  }
}

/*  Checkbox  */
function clearCheckbox() {
  checkboxArray.forEach((item, i) => {
    item.checked = false;
  });
}

function freezeCheckbox(booleon) {
  checkboxArray.forEach((item, i) => {
    item.disabled = booleon;
  });
}

function statusCheckbox() {
  let count = [];
  checkboxArray.forEach((item, i) => {
    if (item.checked == false) count += [i];
  });
  if (count.length == 1) checkboxArray[count].disabled = true;
  else freezeCheckbox(false);
}

function updateCheckbox() {
  checkboxArray.forEach((item, i) => {
    if (item.checked) onCheckbox(i);
  });
}

/*  Order  */
function freezeOrder(booleon, index) {
  let order = document.getElementById("order-by");
  order.options.selectedIndex = index;
  order.disabled = booleon;
}

/*  Other */
function getSelectedIndex(id) {
  return document.getElementById(id).options.selectedIndex;
}

function setDisplayColumn(id, display) {
  for (var item of table.querySelectorAll("td:nth-child(5n+" + id + ")"))
    item.style.display = display;
}

function clearTable() {
  while (table.hasChildNodes())
    table.removeChild(table.lastChild);
}

/*  Format conversion  */
function toDate(string) {
  return new Date(string).toLocaleDateString('ru-RU');
}

function toAmount(string) {
  return string.toLocaleString('ru-RU', { style: 'currency', currency: 'RUB' });
}

/*  Auxiliary  */
function getUnsignedAmount(amount) {
  if (amount >= 0) return [toAmount(amount), "color: green"];
  else return [toAmount(amount * (-1)), "color: red"];
}
