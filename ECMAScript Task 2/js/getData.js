/* Workind */
(function() {
  getData(myStatementData);
}());

/* Get data */
function getData(array) {
  createElement([
    "Дата",
    "Время",
    "Тип",
    "Приход",
    "Расход"
  ], 0);

  array.forEach((item, i) => {
    createElement(function() {
      let date = toDate(item.date.slice(0, 10));
      let time = item.date.slice(11, 19);
      let coming = (item.amount >= 0 ? getUnsignedAmount(item.amount) : "")
      let outgo = (item.amount < 0 ? getUnsignedAmount(item.amount) : "")

      return [
        date,
        time,
        item.type,
        coming,
        outgo
      ];
    }(), item._id);
  });
}
