/* Listener */
function onCheckbox(id, value) {
  statusCheckbox();
  // Set active checkbox.
  if (checkboxArray[id].checked)
    setDisplayColumn(id + 1, "none");

  // Set not active checkbox.
  if (!checkboxArray[id].checked)
    setDisplayColumn(id + 1, "table-cell")
}

function onGroup() {
  clearTable();
  clearCheckbox();
  freezeOrder(false);
  freezeCheckbox(false);

  let selectedIndex = getSelectedIndex("group-by");
  if (selectedIndex == 1) getGroupForDate();
  else getData(myStatementData);
}

function onOrder() {
  clearTable();

  let selectedIndex = getSelectedIndex("order-by");
  if (selectedIndex == 1) getData(sortDataForDate(myStatementData, -1));
  else if (selectedIndex == 2) getData(sortDataForDate(myStatementData, 1));
  else getData(myStatementData);
  // Refresh active checkboxes and hide columns.
  updateCheckbox();
}
