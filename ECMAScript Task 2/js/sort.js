/* Sort data */
function sortDataForDate(array, typeSort) {
  return array.slice().sort((a, b) => (
    new Date(a.date) > new Date(b.date) ? typeSort * 1 : typeSort * (-1))
  );
}
