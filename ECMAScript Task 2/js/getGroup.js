function getGroupForDate() {
  freezeOrder(true, 1);
  freezeCheckbox(true);

  createElement([
    "Дата",
    "Счёт"
  ], 0);

  let tempArray = sortDataForDate(myStatementData, -1);
  for (var i = 0; i < tempArray.length - 1; i++)
    if (tempArray[i].date.slice(0, 10) == tempArray[i + 1].date.slice(0, 10)) {
      mergeRows(tempArray[i], tempArray[i + 1])
      i++;
    } else {
      if (tempArray.length == i + 2) {
        mergeRows(tempArray[i], null);
        mergeRows(tempArray[i + 1], null);
      } else mergeRows(tempArray[i], null);
    }
}

function mergeRows(row, nextRow) {
  createElement([
    toDate(row.date.slice(0, 10)),
    (function() {
      if(nextRow == null) return getUnsignedAmount(row.amount);
      else return getUnsignedAmount(row.amount + nextRow.amount);
    }())
  ], row._id);
}
